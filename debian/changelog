r-bioc-deseq2 (1.46.0+dfsg-2) unstable; urgency=medium

  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Mon, 13 Jan 2025 08:57:33 +0100

r-bioc-deseq2 (1.46.0+dfsg-1) experimental; urgency=medium

  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Thu, 28 Nov 2024 18:15:12 +0100

r-bioc-deseq2 (1.44.0+dfsg-1) unstable; urgency=medium

  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 17 Aug 2024 21:24:14 +0200

r-bioc-deseq2 (1.44.0+dfsg-1~0exp1) experimental; urgency=medium

  * d/{,tests/}control: bump minimum versions of r-bioc-* packages to
    bioc-3.19+ versions

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 06 Aug 2024 13:49:43 +0200

r-bioc-deseq2 (1.44.0+dfsg-1~0exp) experimental; urgency=medium

  * New upstream version
  * d/control: Skip building on 32-bit systems.

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 09 Jul 2024 22:26:32 +0200

r-bioc-deseq2 (1.42.1+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Set upstream metadata fields: Archive.
  * d/control: fix my email address.

 -- Michael R. Crusoe <crusoe@debian.org>  Mon, 11 Mar 2024 15:56:36 +0100

r-bioc-deseq2 (1.42.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)
  Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Thu, 30 Nov 2023 14:28:08 +0100

r-bioc-deseq2 (1.40.2+dfsg-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 27 Jul 2023 15:13:15 +0200

r-bioc-deseq2 (1.38.3+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sat, 21 Jan 2023 14:53:56 +0100

r-bioc-deseq2 (1.38.2+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 19 Dec 2022 16:44:14 +0100

r-bioc-deseq2 (1.38.1+dfsg-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)
  * Reduce piuparts noise in transitions (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 21 Nov 2022 20:25:25 +0100

r-bioc-deseq2 (1.36.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 16 May 2022 20:37:24 +0200

r-bioc-deseq2 (1.34.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete field Contact from debian/upstream/metadata (already present
    in machine-readable debian/copyright).

 -- Andreas Tille <tille@debian.org>  Fri, 26 Nov 2021 08:03:48 +0100

r-bioc-deseq2 (1.32.0+dfsg-3) unstable; urgency=medium

  * Disable reprotest
  * r-bioc-tximeta is now available for tests

 -- Andreas Tille <tille@debian.org>  Tue, 28 Sep 2021 18:46:19 +0200

r-bioc-deseq2 (1.32.0+dfsg-2) unstable; urgency=medium

  [ Nilesh Patra ]
  * d/t/autopkgtest-pkg-r.conf: Add r-bioc-tximportdata
    Closes: #994443

 -- Andreas Tille <tille@debian.org>  Sun, 26 Sep 2021 20:58:45 +0200

r-bioc-deseq2 (1.32.0+dfsg-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Team Upload.
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R to update Build-Depends (3) (routine-update)
  * Set upstream metadata fields: Contact.
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on r-bioc-s4vectors.
  * Provide autopkgtest-pkg-r.conf to make sure testthat will be found
  * Drop debian/tests/control and rely on autopkgtest-pkg-r

  [ Nilesh Patra ]
  * d/control: Versioned B-D as per DESCRIPTION file

 -- Nilesh Patra <nilesh@debian.org>  Tue, 14 Sep 2021 00:18:17 +0530

r-bioc-deseq2 (1.30.1+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Refresh patches

 -- Michael R. Crusoe <crusoe@debian.org>  Wed, 24 Feb 2021 12:20:30 +0100

r-bioc-deseq2 (1.30.0+dfsg-3) unstable; urgency=medium

  * r-bioc-glmgampoi is available now - just enable its usage in tests
    Closes: #976614
  * Standards-Version: 4.5.1 (routine-update)
  * Test-Depends: r-bioc-tximport

 -- Andreas Tille <tille@debian.org>  Mon, 14 Dec 2020 09:06:09 +0100

r-bioc-deseq2 (1.30.0+dfsg-2) unstable; urgency=medium

  * Team upload.
  * debian/patches/fix_autopkgtest.patch: Skip tests based on glmGamPoi

 -- Michael R. Crusoe <crusoe@debian.org>  Wed, 18 Nov 2020 10:16:41 +0100

r-bioc-deseq2 (1.30.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 13 (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Inject checking code that makes autopkgtest fail once r-bioc-glmgampoi
    is available.  Once this is the case the test removal should not be
    done any more

 -- Andreas Tille <tille@debian.org>  Wed, 04 Nov 2020 14:18:25 +0100

r-bioc-deseq2 (1.28.1+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Add salsa-ci file (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Wed, 20 May 2020 17:38:18 +0200

r-bioc-deseq2 (1.26.0+dfsg-2) unstable; urgency=medium

  * Team upload
  * locfit is in Debian, undoing removal.
  * d/control: Standards-Version: 4.5.0
  * d/control: Rules-Requires-Root: no

 -- Steffen Moeller <moeller@debian.org>  Tue, 05 May 2020 00:58:59 +0200

r-bioc-deseq2 (1.26.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Fixed debian/watch for BioConductor
  * debhelper-compat 12
  * Standards-Version: 4.4.1

 -- Dylan Aïssi <daissi@debian.org>  Tue, 12 Nov 2019 21:26:13 +0100

r-bioc-deseq2 (1.24.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * dh-update-R to update Build-Depends
  * rename debian/tests/control.autodep8 to debian/tests/control

 -- Andreas Tille <tille@debian.org>  Thu, 04 Jul 2019 10:18:07 +0200

r-bioc-deseq2 (1.22.2+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.3.0

 -- Dylan Aïssi <daissi@debian.org>  Sun, 06 Jan 2019 16:08:22 +0100

r-bioc-deseq2 (1.22.1+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Fix autopkgtest.

 -- Dylan Aïssi <daissi@debian.org>  Fri, 30 Nov 2018 23:08:56 +0100

r-bioc-deseq2 (1.22.1+dfsg-1) unstable; urgency=medium

  * Team upload.

  [ Andreas Tille ]
  * There is no need to force extra Build-Depends since #900974 was
    closed

  [ Dylan Aïssi ]
  * New upstream version
  * Standards-Version: 4.2.1

 -- Dylan Aïssi <daissi@debian.org>  Sun, 11 Nov 2018 16:53:53 +0100

r-bioc-deseq2 (1.20.0+dfsg-1) unstable; urgency=medium

  * Rebuild for R 3.5 transition
  * dh-update-R to update Build-Depends
  * Remove autogenerated HTML docs since it is including compressed JS

 -- Andreas Tille <tille@debian.org>  Thu, 07 Jun 2018 09:16:33 +0200

r-bioc-deseq2 (1.20.0-2) unstable; urgency=medium

  * Fix Maintainer and Vcs-fields
  * Add comment about really needed Build-Depends not respected by
    dh-update-R

 -- Andreas Tille <tille@debian.org>  Sun, 06 May 2018 07:02:08 +0200

r-bioc-deseq2 (1.20.0-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.4
  * dh-update-R to update Build-Depends

 -- Andreas Tille <tille@debian.org>  Wed, 02 May 2018 09:38:52 +0200

r-bioc-deseq2 (1.18.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version.
  * Fix autopkgtest.
  * Add ref to registries.
  * Standards-Version: 4.1.3.
  * Debhelper 11.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Thu, 01 Mar 2018 00:03:42 +0100

r-bioc-deseq2 (1.18.0-1) unstable; urgency=medium

  * New upstream version
  * Secure URI in watch file

 -- Andreas Tille <tille@debian.org>  Fri, 10 Nov 2017 09:00:02 +0100

r-bioc-deseq2 (1.16.1-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.1.1
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Mon, 02 Oct 2017 22:21:29 +0200

r-bioc-deseq2 (1.14.1-1) unstable; urgency=medium

  * New upstream version
  * debhelper 10
  * d/watch: version=4

 -- Andreas Tille <tille@debian.org>  Sun, 04 Dec 2016 14:11:45 +0100

r-bioc-deseq2 (1.14.0-1) unstable; urgency=medium

  * New upstream version
  * Add myself to uploaders
  * Convert to dh-r

 -- Andreas Tille <tille@debian.org>  Thu, 27 Oct 2016 18:40:20 +0200

r-bioc-deseq2 (1.12.4-1) unstable; urgency=medium

  * Team upload
  * New upstream version
    Closes: #837235

 -- Andreas Tille <tille@debian.org>  Sat, 10 Sep 2016 21:53:51 +0200

r-bioc-deseq2 (1.12.3-2) unstable; urgency=medium

  * Team upload
  * Fix autopkgtest
  * Fix Vcs-URL

 -- Gordon Ball <gordon@chronitis.net>  Wed, 24 Aug 2016 08:39:02 +0200

r-bioc-deseq2 (1.12.3-1) unstable; urgency=medium

  * Initial release (Closes: #828850)

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Mon, 27 Jun 2016 13:42:38 -0700
