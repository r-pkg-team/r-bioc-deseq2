Source: r-bioc-deseq2
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Michael R. Crusoe <crusoe@debian.org>,
           Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-bioc-s4vectors,
               r-bioc-iranges,
               r-bioc-genomicranges,
               r-bioc-summarizedexperiment,
               r-bioc-biocgenerics,
               r-bioc-biobase,
               r-bioc-biocparallel,
               r-cran-matrixstats,
               r-cran-locfit,
               r-cran-ggplot2 (>= 3.4.0),
               r-cran-rcpp,
               r-bioc-matrixgenerics,
               r-cran-rcpparmadillo,
               architecture-is-64-bit
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-bioc-deseq2
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-bioc-deseq2.git
Homepage: https://bioconductor.org/packages/DESeq2/
Rules-Requires-Root: no

Package: r-bioc-deseq2
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: R package for RNA-Seq Differential Expression Analysis
 Differential gene expression analysis based on the negative binomial
 distribution. Estimate variance-mean dependence in count data from
 high-throughput sequencing assays and test for differential expression based
 on a model using the negative binomial distribution.
